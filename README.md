

# Blind Reader Gear suite
This gear suite provides the tools to distribute images from a main project to radiologists, who must then read the image.  This is achieved by answereing a custom set of questions about the image, and/or annotating the image with measurements (ROIs, distances, etc).  These answers/notations can then be recovered back into the main project.

The basic workflow is divided as follows:
1. A main project is curated.  The gear suite distributes on a SESSION by session basis, so ALL acquisitions in a given session will be sent to the radiologists together.  If you have two acquisitions in one session, and you want them to go to different readers, you MUST split them up into their own sessions.
2. A list of readers is provided and the "assign-readers" gears is run.  This list provides consists of valid, existing flywheel users, and the maximum number of cases each user will read.  This creates reader projects for all readers specified in the CSV
3. the "assign-cases" gear is run.  This randomly distributes sessions to the readers.


> :warning: **NOTE**: If you do not want a truly random distribution, you must use `assign-batch-cases`, which takes as input a csv file.  This csv file specifies which session ID's you want to send to which readers.  This allows you complete control over the distribution, so it can suit any need you may have. 

4. "gather-cases" is run to collect all read information provided by the radiologists.

A reader must have read-write access to a specific "reader project" within the designated readers group.

Administrative permission for main Projects, the Readers group, and all reader projects is required for valid execution of all gears. Permissions to the Readers group may be granted to administrators of the project. To ensure that each project administrator can review a readers projects, they should be added as an admin to the Readers Group's Projects Template.

## Objectives

In order to achieve the desired workflow, four separate gears are developed:

1. [**Assign-Readers**](./gears/assign_readers/)

    This gear creates, initializes, and modifies projects with permission for one reader each.  This is done either in bulk with a csv file or with an individual specified in the gear configuration.  All reader projects are created within the “Readers” group.

2. [**Assign-Cases**](./gears/assign_cases/)

    This gear distributes each case of a "Master Project" to three randomly selected distinct reader projects for assessment. Readers are selected without replacement. For specifics about the relationship between the number of cases in a master project and the number of available readers, please see the `assign-cases` [README](./gears/assign_cases/).

3. [**Gather-Cases**](./gears/gather_cases/)

    This gear gathers case assessment status and assessment data into the session of origin.

4. [**Assign-Single-Case**](./gears/assign_single_case/)

    This gear is used for the assignment or reassignment of a specific single case to a single reader.

The gears above keep track of case assignments and assignment status in the metadata associated with each respective Flywheel container.  These metadata are backed up within csv files that are the outputs of both the assign-cases and the gather-cases gears.  Should the metadata in the projects accidentally get corrupted, these csv files can be used to restore the state of the system.

**NOTE:** All gears must be run from within a "Main Project". Attempting to execute any gear from within a reader project will fail.

**NOTE:** No gear in this suite can be run concurrently with any other gear in this suite. This is done to ensure the integrity of the data and metadata at each step.

**NOTE:** Gears run by a user without administrative priviledges on all involved projects will fail.
